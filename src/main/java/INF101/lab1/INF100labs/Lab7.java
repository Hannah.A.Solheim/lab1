package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int rowSum = sumList(grid.get(0));

        ArrayList<Integer> firstCol = new ArrayList<>();
        for (ArrayList<Integer> row : grid){
            firstCol.add(row.get(0));
        }
        int colSum = sumList(firstCol);

        for (ArrayList<Integer> row : grid){
            if (sumList(row) != rowSum){
                return false;
            }
        }

        for (int i = 0; i < grid.size(); i++){
            ArrayList<Integer> column = new ArrayList<>();
            for (ArrayList<Integer> row : grid){
                column.add(row.get(i));
            }
            if (sumList(column) != colSum){
                return false;
            }
        }
        return true;
    }

                
    private static Integer sumList(ArrayList<Integer> list){
        Integer sum = 0;
        for (Integer i : list){
            sum += i;
        }
        return sum;
    }

}
