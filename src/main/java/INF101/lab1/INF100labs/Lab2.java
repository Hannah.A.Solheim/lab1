package INF101.lab1.INF100labs;

import java.util.List;
import java.util.ArrayList;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        List<String> allWords = new ArrayList<>();
        allWords.add(word1);
        allWords.add(word2);
        allWords.add(word3);
        int longest = 0;
        for (String word : allWords){
            if (word.length() > longest){
                longest = word.length();
            }
        }
        for (String word : allWords){
            if (word.length() == longest){
                System.out.println(word);
            }
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0){
            return true;
        }
        if (year % 100 == 0){
            return false;
        }
        if (year % 4 == 0){
            return true;
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0){
            return true;
        }
        return false;
    }

}
