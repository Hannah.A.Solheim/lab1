package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
    }

    public static void multiplesOfSevenUpTo(int n) {
        int multiple = 7;
        while (multiple <= n){
            System.out.println(multiple);
            multiple += 7;
        }
    }

    public static void multiplicationTable(int n) {
        for (int a = 1; a <= n; a++){
            System.out.print(a + ": ");
            for (int b = a; b <= (a*n); b+=a) {
                System.out.print(b + " ");
            }
        System.out.print("\n");
        }
    }

    public static int crossSum(int num) {
        String numbers = num + "";
        
        List<Integer> digits = new ArrayList<>();
        
        for (int a = 0; a < numbers.length(); a++){
            char number = numbers.charAt(a);
            int i = number - '0';
            digits.add(i);
        }

        int sum = 0;

        for (int i : digits){
                sum += i;
        }
        return sum;
    }
}
